#ifndef HTTP_REQUEST
#define HTTP_REQUEST
#include <openssl/err.h>
#include <openssl/ssl.h>

typedef struct
{
    const char *host;
    const char *path;
    const char *body;
    const char *headers;
} HttpRequest;

typedef enum
{
    GET,
    POST,
    DELETE,
    PUT
} HttpMethod;

typedef struct
{
    SSL_CTX *ctx;
    SSL *ssl;
} Session;

void get(HttpRequest *request);
void del(HttpRequest *request);
void post(HttpRequest *request);
void put(HttpRequest *request);
void prepare_request(Session *session, HttpRequest *request, const char *method, const char *headers);
void send_http_request(HttpRequest *request, const char *method);
void http_response(const char *response);
void close_socket(int socket_descriptor);
void close_session(Session *session);
int is_valid_fullpath_url(const char *host, const char *path);
int code(const char *response);
char *headers(const char *response);
double elapsed_time(clock_t start_time, clock_t end_time);
Session *create_session();

#endif
